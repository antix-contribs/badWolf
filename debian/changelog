badwolf (1.1-4+deb10~contribs1) unstable; urgency=medium

  * Added new Control+q to close the window
  * Fixed save bug in manage-favorite script

 -- nXecure <not@telling.you>  Sat, 08 May 2021 19:09:14 +0200

badwolf (1.1-3+deb10~contribs1) unstable; urgency=medium

  * Replaced ddg with qwant as default search engine in the startpage
  * Added new Control+ and Control-zoom keybindings
  * Created autosave and restore options for manage-favorite
  * Updated startpage to reflect new control+/- keybindings

 -- nXecure <not@telling.you>  Sat, 08 May 2021 00:06:38 +0200

badwolf (1.1-2+deb10~contribs1) unstable; urgency=medium

  * Applied the badwolf-search-entry patch

 -- nXecure <not@telling.you>  Wed, 05 May 2021 21:40:32 +0200

badwolf (1.1-1+deb10~contribs1) unstable; urgency=medium

  [ Haelwenn Monnier ]
  * Additions
    * Bookmarks, they are at their early stage, for now it's only
      completion, edition can be done with third-party programs
      (elinks, keditbookmarks, …).
    * Wayland users should also note the following issue:
      https://gitlab.gnome.org/GNOME/gtk/-/issues/699
    * Content-Filters are finally integrated which means that adblock
      extensions aren't needed anymore, you only need to give a policy
      file
    * New translation: de
    * A context_id is now shown before the tab label
  * Changes
    * WebKit's Intelligent Tracking Protection is now enabled
    * Default CFLAGS: Hardening flags have been added, this should
      improve security on some distros
  * Fixes
    * There should be no more memory/object/processes leaks
    * Crashes happening on the file save dialog should have been fixed
    * Printing dialog now correctly is bound to the main window

 -- nXecure <not@telling.you>  Wed, 05 May 2021 13:18:10 +0200

badwolf (1.0.3-4~rev2) unstable; urgency=medium

  * Cleaning startpage so it is better organized (thanks skidoo)
  * Improvements in Favorites manager
  * Control+B will give you the option to save (or update) the link for 
	the current tab.
  * Control+D will open the favorites manager
  * Other usability improvements on the favorites script (thanks PPC)

 -- nXecure <not@telling.you>  Sat, 31 Oct 2020 18:13:00 +0100

badwolf (1.0.3-4) unstable; urgency=medium

  * Fix manage-favorites script. It properly saves links to Favorites now.
  * Made starpage.html look "cooler".
  * Search in startpage defaults to ddg, but google is also an option.

 -- nXecure <not@telling.you>  Tue, 27 Oct 2020 15:35:57 +0100

badwolf (1.0.3-3) unstable; urgency=medium

  * Now the script creates a folder in $HOME/.config/badwolf/
  * It will copy over the startpage.html the first time it launches.
  * Every new tab will open the startpage.html file.
  * Added a gui based on yad to manage favorite links.
  * You can edit favorite links with Control+b from inside badwolf

 -- nXecure <not@telling.you>  Sun, 25 Oct 2020 18:46:08 +0200

badwolf (1.0.3-2) unstable; urgency=medium

  * Added startpage.html for antiX release (thank PPC, he made it possible)

 -- nXecure <not@telling.you>  Sat, 17 Oct 2020 11:30:56 +0200

badwolf (1.0.3-1) unstable; urgency=medium

  * Updated to latest git version
  * Added new dependency libxml2

 -- nXecure <not@telling.you>  Sun, 04 Oct 2020 22:10:34 +0200

badwolf (1.0.2-2) unstable; urgency=medium

  * Updated to latest git version

 -- nXecure <not@telling.you>  Mon, 18 Aug 2020 22:05:15 +0200

badwolf (1.0.2-1) unstable; urgency=medium

  * Initial release
  * Fix build problem (installing bin in wrong directory)
  * Changed to debian standard directory paths

 -- nXecure <not@telling.you>  Mon, 17 Aug 2020 13:18:04 +0200
