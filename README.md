# BadWolf
Minimalist and privacy-oriented WebKitGTK+ browser.

Homepage: <https://hacktivis.me/projects/badwolf>

NEW: Modifications to build .deb packages (locally) for Debian (see **Debian Packaging** entry below).

```
Copyright © 2019-2021 Badwolf Authors <https://hacktivis.me/projects/badwolf>
SPDX-License-Identifier: BSD-3-Clause
```

The name is a reference to BBC’s Doctor Who Tv serie, I took it simply because I wanted to have a specie in the name, like some other web browsers do, but doesn’t go into the “gentle” zone.

## Differencies
Comparing from other small WebKit browsers for unixes found in the wild:

- Independent of environment, should just work if GTK and WebKitGTK does
- Storing data should be:
  - explicit and optionnal (ie. Applying preferences doesn't imply Saving to disk)
  - not queryabe by WebKit (so the web can't use it)
  - done in a standard format (like XBEL for bookmarks)
- Static UI, no element should be added at runtime, this is to avoid potential tracking via viewport changes
- Small codebase, should be possible to read and understand it completely over an afternoon.
- Does not use modal editing (from vi) as that was designed for editing, not browsing
- UTF-8 encoding by default

Motivation from other clients <https://hacktivis.me/articles/www-client%20are%20broken>

## Contributing
### Translations
You need to have gettext installed. If you want a GUI, poedit exists and Weblate is a good web platform that I might consider hosting at some point.

- Syncing POT file with the source code: ``make po/messages.pot``
- Syncing PO file with the POT file: ``make po/de.po``
- Initialising a new PO file (example for German, `de_DE`): ``msginit -l de_DE -i po/messages.pot -o po/de.po``

## Contacts / Discussions
- IRC: `#badwolf-browser` on FreeNode
- Matrix (bridge): <https://matrix.to/#/#freenode_#badwolf-browser:matrix.org>

## Repositories
### git
- Main: <https://hacktivis.me/git/badwolf/>, <git://hacktivis.me/git/badwolf.git>
- Mirror: <https://gitlab.com/lanodan/badWolf.git>, this one can also be used if you prefer tickets/PRs over emails

### release assets
- Main: <https://hacktivis.me/releases/>
- Mirror: <https://gitlab.com/lanodan/badWolf/tags>

- `*.tar.*` files are tarballs archives to be extracted with a program like `tar(1)`, GNU tar and LibArchive bsdtar are known to work.
- `*.sig` files are OpenPGP signatures done with my [key](https://hacktivis.me/key.asc)(`DDC9 237C 14CF 6F4D D847  F6B3 90D9 3ACC FEFF 61AE`).
- `*.sign` files are minisign (OpenBSD `signify(1)` compatible) signatures, they key used for it can be found at <https://hacktivis.me/releases/signify/> as well as other places (feel free to ping me to get it)

## Debian Packaging
This source has been modified for easy .deb package building.
Building has been tested on a debian buster distro (antiX 19.2.1 base) and it seems to work.
Simple instructions below for personal use.

**0. Download source**

Download this source and unpack it (if not already) to a separate folder with easy access.
Example: unpack to `~/badwolf/` folder.

**1. Install dependencies**

Badwolf build dependencies: `libsoup2.4-dev libwebkit2gtk-4.0-dev libxml2-dev`
Packaging prerequisites: `build-essential fakeroot devscripts debhelper`

One-liner:
~~~
sudo apt install build-essential fakeroot devscripts debhelper libsoup2.4-dev libwebkit2gtk-4.0-dev libxml2-dev
~~~
**2. Building .deb package**

The easy way is navigating to the folder that contains the source, open a terminal there, and run:
~~~
debuild -b -uc -us
~~~
It will build the .deb package for your architecture and save it "outside" the folder (one level above).
Example: for 64 bits, it will create a `badwolf_1.0.3-1_amd64.deb` file
If you want to build for other architectures you will need to use other tools like sbuild or pbuilder, or use a virtual machine running your desired architecture.

**3. Install**

You can install using gdebi, your software-center program or from terminal. Example below:
~~~
sudo apt install /path/to/badwolf_1.0.3-3_amd64.deb
~~~

**4. Extra Dependencies**

Now, for installation, it extra dependencies were added: yad for the favorite link manager; gstreamer1.0-gl to try to fix freezing that comes after skipping ads in youtube.

## Manual Installation
Dependencies are:

- C11 Compiler (such as clang or gcc)
- [WebKitGTK](https://webkitgtk.org/), only the latest stable is supported
- [libxml-2.0](http://www.xmlsoft.org/), no known version limitation
- POSIX make with extension for shell in variables (works with GNU, {Net,Free,Open}BSD)
- A pkg-config implementation (pkgconf is recommended)
- (optionnal) gettext implementation (such as GNU Gettext)

Compilation is done with `make`, install with `make install` (`DESTDIR` and `PREFIX` environment variables are supported, amongs other common ones). An example AppArmor profile is provided at `usr.bin.badwolf`, please do some long runtime checks before shipping it or a modified version, help can be provided but with no support.

You'll also need inkscape (command line only) if you want to regenerate the icons, for example after modifying them or adding a new size. These aren't needed for normal installation as it is bundled.

## Notes
Most of the privacy/security stuff will be done with patches against WebKit as quite a lot isn’t into [WebKitSettings](https://webkitgtk.org/reference/webkit2gtk/stable/WebKitSettings.html) and with generic WebKit extensions that should be resuseable.
